//
//  SongFinderTests.swift
//  SongFinderTests
//
//  Created by Marcin Obolewicz on 08/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import XCTest
import RxCocoa
import RxSwift

@testable import SongFinder

class ProviderFailMock: SpotifyProviderProtocol {
    func requestToken() -> Observable<Token> {
        return Observable<Token>.error(ApiError.requestError)
    }
    
    func request(query: String) -> Observable<[SongListItem]> {
        return Observable<[SongListItem]>.error(ApiError.requestError)
    }
}

class ProviderMock: ItunesProviderProtocol {
    private var songs =
        [ItunesSong(artistName: "artist1", trackName: "track1", collectionName: "album1"),
         ItunesSong(artistName: "artist1", trackName: "song1", collectionName: "album1")]
    
    func request(query: String) -> Observable<[SongListItem]> {
        return Observable.just(songs)
    }
}

class SongFinderTests: XCTestCase {

    func testApiError() {
        let spotifyViewModel = SpotifySongsListViewModel(service: ProviderFailMock())
        let itunesViewModel = ItunesSongsListViewModel(service: ProviderMock())
        
        let vc = SongsTableViewController(itunesViewModel: itunesViewModel, spotifyViewModel: spotifyViewModel)
        
        let _ = vc.view
        XCTAssertEqual(vc.allSongs.value.count, 0)
    }
}
