//
//  SongsListViewModel.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 08/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol SongsListViewModelProtocol {
    func fetchData(query: String)
    var songs: PublishSubject<[SongListItem]> { get }
    var disposeBag: DisposeBag { get }
}

class ItunesSongsListViewModel: SongsListViewModelProtocol {
    let disposeBag = DisposeBag()
    var songs: PublishSubject<[SongListItem]> = PublishSubject()
    var service: ItunesProviderProtocol
    
    init(service: ItunesProviderProtocol) {
        self.service = service
    }
    
    func fetchData(query: String) {
        self.service.request(query: query)
            .bind(to: self.songs)
            .disposed(by: self.disposeBag)
    }
}
