//
//  SongsTableViewController.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 08/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SongsTableViewController: UITableViewController {
    let itunesViewModel: SongsListViewModelProtocol
    let spotifyViewModel: SongsListViewModelProtocol
    var allSongs = BehaviorRelay<[SongListItem]>(value: [])
    let disposeBag = DisposeBag()
    
    init(itunesViewModel: SongsListViewModelProtocol, spotifyViewModel: SongsListViewModelProtocol) {
        self.itunesViewModel = itunesViewModel
        self.spotifyViewModel = spotifyViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSearchView()
        self.setupView()
        self.bindTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesSearchBarWhenScrolling = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesSearchBarWhenScrolling = true
        }
    }
    
    private func setupSearchView() {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Type Artists Name"
        self.navigationItem.searchController = searchController
    }
    
    private func setupView() {
        self.view.backgroundColor = UIColor.white
    }
    
    private func bindTableView() {
        self.tableView.dataSource = nil
        UITableViewCell.register(to: self.tableView)
        
        Observable.combineLatest(self.spotifyViewModel.songs, self.itunesViewModel.songs).map { [$0.0, $0.1].flatMap({$0})}
            .bind(to: self.allSongs)
            .disposed(by: self.disposeBag)
        
        self.allSongs
            .bind(to: self.tableView.rx.items(cellIdentifier: UITableViewCell.className, cellType: UITableViewCell.self)) { index, model, cell in
                cell.textLabel?.text = model.name
                cell.contentView.backgroundColor = model.source == .spotify ? UIColor.green : UIColor.blue
            }
            .disposed(by: self.disposeBag)
        
        self.tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                self?.tableView.deselectRow(at: indexPath, animated: true)
            })
            .disposed(by: self.disposeBag)
        
        self.tableView.rx.modelSelected(SongListItem.self)
            .subscribe(onNext: { [weak self] song in
                let viewController = SongDetailsViewController(song: song)
                self?.navigationItem.searchController?.dismiss(animated: false)
                self?.navigationController?.pushViewController(viewController, animated: true)
            })
            .disposed(by: self.disposeBag)
        
    }
    
    private func fetchAll(query: String) {
        self.itunesViewModel.fetchData(query: query)
        self.spotifyViewModel.fetchData(query: query)
    }
}

extension SongsTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text {
            self.fetchAll(query: text)
        }
    }
}

extension SongsTableViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.fetchAll(query: "")
    }
}
