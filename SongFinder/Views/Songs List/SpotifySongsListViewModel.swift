//
//  SpotifySongsListViewModel.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 09/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class SpotifySongsListViewModel: SongsListViewModelProtocol {
    let disposeBag = DisposeBag()
    var songs: PublishSubject<[SongListItem]> = PublishSubject()
    var service: SpotifyProviderProtocol
    
    init(service: SpotifyProviderProtocol) {
        self.service = service
        self.getToken()
    }
    
    func getToken() {
        self.service.requestToken()
            .subscribe(onNext: { _ in
            })
            .disposed(by: self.disposeBag)
    }
    
    func fetchData(query: String) {
        self.service.request(query: query)
            .bind(to: self.songs)
            .disposed(by: self.disposeBag)
    }
}
