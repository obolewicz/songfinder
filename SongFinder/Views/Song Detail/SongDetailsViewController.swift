//
//  SongDetailsViewController.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 09/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import UIKit
import SnapKit

class SongDetailsViewController: UIViewController {
    let song: SongListItem
    
    private let artistLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 19)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    private let trackLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 19)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    private let albumLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 19)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    init(song: SongListItem) {
        self.song = song
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.fillData()
    }
    
    private func setupView() {
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(artistLabel)
        self.view.addSubview(trackLabel)
        self.view.addSubview(albumLabel)
        
        self.artistLabel.snp.makeConstraints {
            $0.top.equalTo(self.view).offset(80)
            $0.bottom.equalTo(self.trackLabel.snp.top)
            $0.height.equalTo(100)
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
        
        self.trackLabel.snp.makeConstraints {
            $0.bottom.equalTo(self.albumLabel.snp.top)
            $0.height.equalTo(100)
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
        
        self.albumLabel.snp.makeConstraints {
            $0.top.equalTo(self.trackLabel.snp.bottom)
            $0.height.equalTo(100)
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
    }
    
    private func fillData() {
        self.artistLabel.text = self.song.artistsName
        self.trackLabel.text = self.song.name
        self.albumLabel.text = self.song.albumName
    }
}
