//
//  DecodeHelper.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 08/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import Foundation

class DecodeHelper<Item: Codable> {
    static func decode(data: Data) -> Item? {
        let jsonDecoder = JSONDecoder()
//        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        do {
            let value = try jsonDecoder.decode(Item.self, from: data)
            return value
        } catch {
            return nil
        }
    }
}

