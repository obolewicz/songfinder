//
//  Api.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 08/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import Foundation

public enum ApiError: Error {
    case decodingError
    case urlError
    case requestError
    case authorizationError
}

extension ApiError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .decodingError:
            return "Deocding error"
        case .urlError:
            return "Url error"
        case .requestError:
            return "Request error"
        case .authorizationError:
            return "authorizationError"
        }
    }
}
