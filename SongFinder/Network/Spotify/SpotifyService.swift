//
//  SpotifyService.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 08/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya
import Alamofire

enum SpotifyService {
    case postAuth
    case songsSearch(query: String, token: Token)
}

extension SpotifyService: TargetType, AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        switch self {
        case .songsSearch:
            return .bearer
        case .postAuth:
            return .basic
        }
    }
    
    var baseURL: URL {
        switch self {
        case .postAuth:
            return URL(string: "https://accounts.spotify.com/api/")!
        case .songsSearch:
            return URL(string: "https://api.spotify.com/v1/")!
        }
    }
    
    var path: String {
        switch self {
        case .postAuth:
            return "token"
        case .songsSearch:
            return "search"
        }
    }
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .postAuth:
            return .post
        case .songsSearch:
            return .get
        }
    }
    
    var sampleData: Data {
        return "".utf8Encoded
    }
    
    var task: Task {
        
        switch self {
        case let .songsSearch(query, _):
            let term = query == "" ? "*" : query
            return .requestParameters(parameters: ["q": "artist:\(term)", "type": "track"], encoding: URLEncoding.default)
        case .postAuth:
            return .requestCompositeParameters(bodyParameters: ["grant_type": "client_credentials", ], bodyEncoding: URLEncoding.httpBody, urlParameters: [:])
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .postAuth:
            if let authData = Request.authorizationHeader(user: "8396c43e36fe4708b930122a2dcd9045", password: "a34d27fc66b2497bbce667ccffa60d4a") {
                return [
                    "Content-type": "application/x-www-form-urlencoded",
                    authData.key: authData.value]
            }
            return nil
        case let .songsSearch(_, token):
            return ["Authorization": "\(token.tokenType!) \(token.accessToken!)"]
        }
    }
}

private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}

