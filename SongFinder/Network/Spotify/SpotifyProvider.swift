//
//  SpotifyProvider.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 08/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import Moya

class SpotifyProvider: SpotifyProviderProtocol {
    let provider = MoyaProvider<SpotifyService>()
    
    var token: Token? = nil
    
    func requestToken() -> Observable<Token> {
        return Observable.create({ observer in
            let cancellable = self.provider.request(.postAuth) { result in
                switch result {
                case let .success(moyaResponse):
                    if 200 ..< 300 ~= moyaResponse.statusCode {
                        let jsonDecoder = JSONDecoder()
                        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
                        do {
                            let value: Token = try jsonDecoder.decode(Token.self, from: moyaResponse.data)
                            self.token = value
                            observer.onNext(value)
                        } catch {
                            observer.onError(ApiError.decodingError)
                        }
                    }
                    else {
                        observer.onError(ApiError.requestError)
                    }
                case let .failure(error):
                    observer.onError(error)
                }
            }
            return Disposables.create {
                cancellable.cancel()
            }
        })
    }
    
    func request(query: String) -> Observable<[SongListItem]> {
        guard let token = self.token else {
            return Observable.error(ApiError.requestError)
        }
        return Observable.create({ observer in
            let cancellable = self.provider.request(.songsSearch(query: query, token: token)) { result in
                switch result {
                case let .success(moyaResponse):
                    if 200 ..< 300 ~= moyaResponse.statusCode {
                        let jsonDecoder = JSONDecoder()
                        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
                        do {
                            let value: SpotifyResponse = try jsonDecoder.decode(SpotifyResponse.self, from: moyaResponse.data)
                            observer.onNext(value.tracks?.items ?? [])
                        } catch {
                            observer.onError(ApiError.decodingError)
                        }
                    }
                    else {
                        observer.onError(ApiError.requestError)
                    }
                case let .failure(error):
                    observer.onError(error)
                }
            }
            return Disposables.create {
                cancellable.cancel()
            }
        })
    }
}

