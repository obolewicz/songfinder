//
//  GetListService.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 08/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya
import Alamofire

enum ItunesService {
    case songsSearch(query: String)
}

extension ItunesService: TargetType {
    var baseURL: URL {
        return URL(string: "https://itunes.apple.com/")!
    }
    
    var path: String {
        return "search"
    }
    
    var method: Alamofire.HTTPMethod {
        return .get
    }
    
    var sampleData: Data {
        return "".utf8Encoded
    }
    
    var task: Task {
        
        switch self {
        case let .songsSearch(query):
            let term = query == "" ? "*" : query
            return .requestParameters(parameters: ["term": term, "entity": "musicTrack", "attribute": "allArtistTerm"], encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
}

private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
