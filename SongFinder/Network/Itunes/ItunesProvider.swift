//
//  ItunesProvider.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 08/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import Moya

class ItunesProvider: ItunesProviderProtocol {
    let provider = MoyaProvider<ItunesService>()
    
    func request(query: String) -> Observable<[SongListItem]> {
        return Observable.create({ observer in
            let cancellable = self.provider.request(.songsSearch(query: query)) { result in
                switch result {
                case let .success(moyaResponse):
                    if 200 ..< 300 ~= moyaResponse.statusCode {
                        do {
                            let jsonDecoder = JSONDecoder()
                            jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
                            let value: ItunesResponse = try jsonDecoder.decode(ItunesResponse<ItunesSong>.self, from: moyaResponse.data)
                            observer.onNext(value.results ?? [])
                        } catch {
                            observer.onError(ApiError.decodingError)
                        }
                    }
                    else {
                        observer.onError(ApiError.requestError)
                    }
                case let .failure(error):
                    observer.onError(error)
                }
            }
            return Disposables.create {
                cancellable.cancel()
            }
        })
    }
}

