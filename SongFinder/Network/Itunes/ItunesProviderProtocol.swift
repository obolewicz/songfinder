//
//  GetListServiceProtocol.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 08/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol ItunesProviderProtocol {
    func request(query: String) -> Observable<[SongListItem]>
}
