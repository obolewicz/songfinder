//
//  Token.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 08/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import Foundation

struct Token: Codable {
    var accessToken: String?
    var tokenType: String?
}
