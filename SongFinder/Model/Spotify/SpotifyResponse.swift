//
//  SpotifyResponse.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 09/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import Foundation

struct SpotifyResponse: Codable {
    var tracks: Tracks?
}

struct Tracks: Codable {
    var items: [Track]?
}

struct Track: Codable {
    var name: String
    var album: Album?
    var artists: [Artist]?
    
    var source: SongSource {
        return .spotify
    }
}

extension Track: SongListItem {
    var artistsName: String {
        if let artits = self.artists {
            let artistNames = artits.compactMap { $0.name }
            return NSArray(array: artistNames).componentsJoined(by: ",")
        }
        return ""
    }
    
    var albumName: String {
        return self.album?.name ?? ""
    }
}

struct Album: Codable {
    var name: String?
}

struct Artist: Codable {
    var name: String?
}
