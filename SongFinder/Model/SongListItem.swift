//
//  SongListItem.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 08/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import Foundation

enum SongSource {
    case spotify
    case itunes
}

protocol SongListItem {
    var name: String { get }
    var artistsName: String { get }
    var albumName: String { get }
    var source: SongSource { get }
}
