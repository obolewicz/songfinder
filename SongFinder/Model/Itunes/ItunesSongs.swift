//
//  ItunesSongs.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 08/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import Foundation

struct ItunesSong: Codable {
    var artistName: String?
    var trackName: String?
    var collectionName: String?
}

extension ItunesSong: SongListItem {
    var artistsName: String {
        return self.artistName ?? ""
    }
    
    var albumName: String {
        return self.collectionName ?? ""
    }
    
    var name: String {
        get {
            return self.trackName ?? ""
        }
    }
    var source: SongSource {
        return .itunes
    }
}
