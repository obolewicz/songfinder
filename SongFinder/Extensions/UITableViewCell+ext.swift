//
//  UITableViewCell+ext.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 08/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static func registerNib(to tableView: UITableView){
        let contactNib = UINib(nibName: self.className, bundle: nil)
        tableView.register(contactNib, forCellReuseIdentifier: self.className)
    }
    
    static func register(to tableView: UITableView){
        tableView.register(self, forCellReuseIdentifier: self.className)
    }
}
