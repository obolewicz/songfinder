//
//  UIView+ext.swift
//  SongFinder
//
//  Created by Marcin Obolewicz on 08/08/2019.
//  Copyright © 2019 obolewicz. All rights reserved.
//

import UIKit

extension UIView {
    var className: String { return String(describing: type(of: self)) }
    static var className: String { return String(describing: self) }
}
